# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [4.1.8075] - 2022-02-09
* Convert to C#.

## [4.1.8070] - 2022-02-04
* Targeting Visual Studio 2022, C# 10 and .NET 6.0.
* Update NuGet packages.
* Remove unused references. 
* Update build version.
* Display version file when updating build version.
* Uses LLBLGEN NuGet package.

## [4.1.6667] - 2018-04-03
* 2018 release.

## [4.0.5878] - 2016-02-04
* Adds collection display and management.

## [4.0.5866] - 2016-01-23
* Updates to .NET 4.5.2

## [3.0.5145] - 2014-02-01
* Adds extension to determine if entity is not new or dirty.

## [3.0.5126] - 2014-01-13
* Tagged as 2014.

## [3.0.5030] - 2013-10-09
* Updates to version 3.0 to match the rest of the LLBLGEN releases that use the Core libraries.

## [2.2.4710] - 2012-11-23
* Removes .VB tags from assemblies.

## [2.2.4538] - 2012-06-04
* Adds support for LLBLGEN 3.5.

## [2.2.4498] - 2012-04-25
* Renames extension model. Adds Hide Columns extension for grid.

## [2.2.4490] - 2012-04-17
* Implements code analysis rules for .NET 4.0.

## [2.2.4300] - 2011-10-10
* Separated from the Publisher libraries.

## [2.2.4232] - 2011-08-03
* Standardize code elements and documentation.

## [2.1.4213] - 2011-07-15
* Simplifies the assembly information.

## [2.1.4167] - 2011-05-30
* Fixes bug in restoring auto fetch.

## [2.1.4163] - 2011-05-26
* Disables auto fetch while notifying of entity changes.

## [2.1.4157] - 2011-05-20
* Upgrades to LLBLGEN 3.1.

## [2.0.4102] - 2011-03-26
* Adds New Entity to clear the entity.

## [2.0.3997] - 2010-12-11
* Renames all classes to use the LLBLGEN notations for designating Entity2 as Adaptor entities and Entity as Self-Servicing Entities. Adds support for Self-Servicing entities.

## [1.0.3989] - 2010-12-03
* Creates a new context for a new entity.

## [1.0.3975] - 2010-11-19
* Created.

\(C\) 2010 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
[8075] - end branch vb2cs
```
[4.1.8075]: https://bitbucket.org/davidhary/vs.data.llblgen.git
