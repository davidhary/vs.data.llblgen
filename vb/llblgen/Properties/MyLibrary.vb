Imports System.ComponentModel
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Data + &H2

        Public Const AssemblyTitle As String = "LLBLGen ORM Library"
        Public Const AssemblyDescription As String = "LLBLGen Object Relation Mapping Library"
        Public Const AssemblyProduct As String = "LLBLGen"

    End Class

    ''' <summary> Values that represent project trace event identifiers. </summary>
    Public Enum ProjectTraceEventId
        <Description("Not specified")> None
        <Description("LLBLGen Core")> LLBLGenCore = isr.Core.ProjectTraceEventId.Data + &H2
        <Description("LLBLGen Publishers")> LLBLGenPublishers = isr.Core.ProjectTraceEventId.Data + &H3
        <Description("LLBLGen Self Servicing")> LLBLGenSelfServicing = isr.Core.ProjectTraceEventId.Data + &H4
    End Enum

End Namespace



