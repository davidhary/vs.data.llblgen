Imports System.Runtime.CompilerServices
Imports SD.LLBLGen.Pro.ORMSupportClasses
Namespace PrefetchPathExtensions
    ''' <summary> Prefetch path Extensions. </summary>
    ''' <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para> </remarks>
    <Extension()>
    Public Module Methods

        ''' <summary> Determines whether the prefetch path contains the specified path. </summary>
        ''' <param name="path">      The path. </param>
        ''' <param name="candidate"> The candidate. </param>
        ''' <returns> <c>True</c> if the prefetch path contains the specified path; otherwise,
        ''' <c>False</c>. </returns>
        <Extension()>
        Public Function Contains(ByVal path As IPrefetchPath, ByVal candidate As IPrefetchPathElement) As Boolean
            If path Is Nothing OrElse candidate Is Nothing Then
                Return False
            ElseIf path.Count = 0 Then
                Return False
            Else
                For i As Integer = 0 To path.Count - 1
                    If path.Item(i).Equals(candidate) Then
                        Return True
                    End If
                Next
                Return False
            End If
        End Function

        ''' <summary> Adds the specified candidate path or selects it if it already exists. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="path">      The path. </param>
        ''' <param name="candidate"> The candidate. </param>
        ''' <returns> <see cref="IPrefetchPathElement">Prefetch path element.</see> </returns>
        <Extension()>
        Public Function AddSelect(ByVal path As IPrefetchPath, ByVal candidate As IPrefetchPathElement) As IPrefetchPathElement

            If path Is Nothing Then
                Throw New ArgumentNullException(NameOf(path))
            Else
                Return If(path.Contains(candidate), candidate, path.Add(candidate))
            End If
        End Function

        ''' <summary> Adds the specified candidate path or selects it if it already exists. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="path">             The path. </param>
        ''' <param name="candidate">        The candidate. </param>
        ''' <param name="additionalSorter"> The additional sorter. </param>
        ''' <returns> <see cref="IPrefetchPathElement">Prefetch path element.</see> </returns>
        <Extension()>
        Public Function AddSelect(ByVal path As IPrefetchPath, ByVal candidate As IPrefetchPathElement,
                                  ByVal additionalSorter As ISortExpression) As IPrefetchPathElement

            If path Is Nothing Then
                Throw New ArgumentNullException(NameOf(path))
            Else
                Return If(path.Contains(candidate), candidate, path.Add(candidate, Nothing, Nothing, Nothing, additionalSorter))
            End If
        End Function

        ''' <summary> Adds the specified candidate path or selects it if it already exists. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="path">                      The path. </param>
        ''' <param name="candidate">                 The candidate. </param>
        ''' <param name="maxAmountOfItemsToReturn">  The max amount of items to return. </param>
        ''' <param name="additionalFilter">          The additional filter. </param>
        ''' <param name="additionalFilterRelations"> The additional filter relations. </param>
        ''' <param name="additionalSorter">          The additional sorter. </param>
        ''' <returns> <see cref="IPrefetchPathElement">Prefetch path element.</see> </returns>
        <Extension()>
        Public Function AddSelect(ByVal path As IPrefetchPath, ByVal candidate As IPrefetchPathElement,
                                  ByVal maxAmountOfItemsToReturn As Integer, ByVal additionalFilter As IPredicateExpression,
                                  ByVal additionalFilterRelations As IRelationCollection, ByVal additionalSorter As ISortExpression) As IPrefetchPathElement

            If path Is Nothing Then
                Throw New ArgumentNullException(NameOf(path))
            Else
                Return If(path.Contains(candidate),
                    candidate,
                    path.Add(candidate, maxAmountOfItemsToReturn, additionalFilter, additionalFilterRelations, additionalSorter))
            End If
        End Function
    End Module

End Namespace
