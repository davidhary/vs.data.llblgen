﻿Imports System.Runtime.CompilerServices
Imports SD.LLBLGen.Pro.ORMSupportClasses
Imports System.Windows.Forms
Imports isr.Data.LLBLGen.AdapterEntityExtensions
Imports isr.Data.LLBLGen.SelfServicingEntityExtensions
Namespace DataGridViewExtensions

    ''' <summary> Includes data grid view extensions for LLBLGEN Entities. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2010-11-19, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " GRID "

        ''' <summary> Hides all columns. </summary>
        ''' <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        <Extension()>
        Public Sub HideColumns(ByVal grid As DataGridView)
            If grid IsNot Nothing Then
                For Each column As DataGridViewColumn In grid.Columns
                    column.Visible = False
                Next
            End If
        End Sub

        ''' <summary> Hides the columns by the <paramref name="columnNames">column names</paramref>. </summary>
        ''' <param name="grid">        The grid. </param>
        ''' <param name="columnNames"> The column names. </param>
        <Extension()>
        Public Sub HideColumns(ByVal grid As DataGridView, ByVal columnNames As String())
            If grid IsNot Nothing Then
                For Each column As DataGridViewColumn In grid.Columns
                    If Not columnNames.Contains(column.Name) Then
                        column.Visible = False
                    End If
                Next
            End If
        End Sub

        ''' <summary> Visible column count. </summary>
        ''' <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        ''' <returns> The visible column count. </returns>
        <Extension()>
        Public Function VisibleColumnCount(ByVal grid As DataGridView) As Integer
            Dim count As Integer = 0
            If grid IsNot Nothing Then
                For Each column As DataGridViewColumn In grid.Columns
                    If column.Visible Then
                        count += 1
                    End If
                Next
            End If
            Return count
        End Function

#End Region

#Region " ADAPTER "

        ''' <summary> Hides columns that are not defined as entity fields. </summary>
        ''' <param name="grid">   The <see cref="DataGridView">grid</see> </param>
        ''' <param name="entity"> Specifies the entity displayed in the data grid. </param>
        <Extension()>
        Public Sub HideColumnsByEntityFields(ByVal grid As DataGridView, ByVal entity As IEntity2)
            If grid IsNot Nothing Then
                grid.HideColumns(entity.GetFieldNames)
            End If
        End Sub

#End Region

#Region " SELF SERVICING "

        ''' <summary> Hides columns that are not defined as entity fields. </summary>
        ''' <param name="grid">   The <see cref="DataGridView">grid</see> </param>
        ''' <param name="entity"> Specifies the entity displayed in the data grid. </param>
        <Extension()>
        Public Sub HideColumnsByEntityFields(ByVal grid As DataGridView, ByVal entity As IEntity)
            If grid IsNot Nothing Then
                grid.HideColumns(entity.GetFieldNames)
            End If
        End Sub

#End Region

    End Module
End Namespace

