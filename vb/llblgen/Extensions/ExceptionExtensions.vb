﻿Imports System.Runtime.CompilerServices

Namespace ExceptionExtensions

    ''' <summary> Adds exception data for building the exception full blown report. </summary>
    ''' <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Adds an exception data to 'exception'. </summary>
        ''' <param name="value">     The value. </param>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Private Function AddExceptionData(ByVal value As System.Exception, ByVal exception As System.Data.SqlClient.SqlException) As Boolean
            If exception IsNot Nothing Then
                Dim count As Integer = value.Data.Count
                value.Data.Add($"{count}-ErrorCode", exception.ErrorCode)
                If exception.Errors?.Count > 0 Then
                    For Each err As System.Data.SqlClient.SqlError In exception.Errors
                        value.Data.Add($"{count}-Error{err.Number}", err.ToString)
                    Next
                End If
            End If
            Return exception IsNot Nothing
        End Function

        ''' <summary> Adds an exception data to 'exception'. </summary>
        ''' <param name="value">     The value. </param>
        ''' <param name="exception"> Details of the exception. </param>
        Private Function AddExceptionData(ByVal value As System.Exception, ByVal exception As SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException) As Boolean
            If exception IsNot Nothing Then
                value.Data.Add($"{value.Data.Count}-Query", exception.QueryExecuted)
            End If
            Return exception IsNot Nothing
        End Function

        ''' <summary> Adds exception data from the specified exception. </summary>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        <Extension>
        Public Function AddExceptionData(ByVal exception As System.Exception) As Boolean
            Return Methods.AddExceptionData(exception, TryCast(exception, System.Data.SqlClient.SqlException)) OrElse
                   Methods.AddExceptionData(exception, TryCast(exception, SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException))
        End Function

        ''' <summary> Adds an exception data. </summary>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Private Function AddExceptionDataThis(ByVal exception As System.Exception) As Boolean
            Return Methods.AddExceptionData(exception) OrElse
                   isr.Core.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData(exception)
        End Function

        ''' <summary> Converts a value to a full blown string. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception) As String
            Return Methods.ToFullBlownString(value, Integer.MaxValue)
        End Function

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="level"> The level. </param>
        ''' <returns> The given data converted to a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception, ByVal level As Integer) As String
            Return isr.Core.ExceptionExtensions.ExceptionExtensionMethods.ToFullBlownString(value, level, AddressOf AddExceptionDataThis)
        End Function

    End Module

End Namespace

