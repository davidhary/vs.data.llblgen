﻿Imports System.Runtime.CompilerServices
Imports SD.LLBLGen.Pro.ORMSupportClasses
Namespace EntityCoreExtensions

    ''' <summary> Includes extensions for LLBLGEN Core Entities. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2010-11-19, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " ENTITY CORE "

        ''' <summary> Determines if the entity is not new or dirty. </summary>
        ''' <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        ''' interface</see> </param>
        ''' <returns> <c>True</c>  if the entity is not new or dirty; Otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IsClean(ByVal entity As IEntityCore) As Boolean
            Return entity IsNot Nothing AndAlso Not (entity.IsNew OrElse entity.IsDirty)
        End Function

        ''' <summary> Query if 'entity' is stored. </summary>
        ''' <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        '''                       interface</see> </param>
        ''' <returns> <c>true</c> if stored; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function IsStored(ByVal entity As IEntityCore) As Boolean
            Return entity IsNot Nothing AndAlso Not entity.IsNew
        End Function

        ''' <summary> Query if 'entity' is nothing or new. </summary>
        ''' <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        '''                       interface</see> </param>
        ''' <returns> <c>true</c> if nothing or new; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function IsNothingOrNew(ByVal entity As IEntityCore) As Boolean
            Return entity Is Nothing OrElse entity.IsNew
        End Function

        ''' <summary> Query if 'entity' is not stored. </summary>
        ''' <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        '''                       interface</see> </param>
        ''' <returns> <c>true</c> if not stored; otherwise <c>false</c> </returns>
        <Extension()>
        Public Function IsNotStored(ByVal entity As IEntityCore) As Boolean
            Return entity IsNot Nothing AndAlso entity.IsNew
        End Function

#End Region

    End Module
End Namespace

