﻿Imports System.Runtime.CompilerServices
Imports SD.LLBLGen.Pro.ORMSupportClasses
Namespace AdapterEntityExtensions

    ''' <summary> Includes extensions for Adapter LLBLGEN Entities. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2010-11-19, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " ADAPTER ENTITY (IEntity2) "

        ''' <summary> Returns the list of field names. </summary>
        ''' <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        ''' interface</see> </param>
        ''' <returns> The field names. </returns>
        <Extension()>
        Public Function GetFieldNames(ByVal entity As IEntity2) As String()
            Dim l As New List(Of String)
            If entity IsNot Nothing Then
                For Each field As IEntityField2 In entity.Fields
                    l.Add(field.Name)
                Next
            End If
            Return l.ToArray
        End Function

        ''' <summary> Returns the list of primary key field names. </summary>
        ''' <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        ''' interface</see> </param>
        ''' <returns> The primary key field names. </returns>
        <Extension()>
        Public Function GetPrimaryKeyFieldNames(ByVal entity As IEntity2) As String()
            Dim l As New List(Of String)
            If entity IsNot Nothing Then
                For Each field As IEntityField2 In entity.PrimaryKeyFields
                    l.Add(field.Name)
                Next
            End If
            Return l.ToArray
        End Function

#End Region

    End Module
End Namespace

