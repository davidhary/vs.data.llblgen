<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EntityCollectionManager

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim dataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me._EntityCollectionGrid = New CollectionDataGridView()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolStripContainer = New System.Windows.Forms.ToolStripContainer()
        Me._BottomToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SaveButtonSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._PrintButtonSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._ActionsButtonSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._BottomProgressBarSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._BottomProgressBar = New System.Windows.Forms.ToolStripProgressBar()
        Me._BottomStatusLabelSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._BottomStatusLabel = New System.Windows.Forms.ToolStripLabel()
        Me._TopToolStrip = New System.Windows.Forms.ToolStrip()
        Me._TitleLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ExportButtonSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me._RefreshButton = New System.Windows.Forms.ToolStripButton()
        Me._SaveButton = New System.Windows.Forms.ToolStripButton()
        Me._PrintButton = New System.Windows.Forms.ToolStripButton()
        Me._ExportButton = New System.Windows.Forms.ToolStripButton()
        Me._ActionsButton = New System.Windows.Forms.ToolStripButton()
        CType(Me._EntityCollectionGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ToolStripContainer.BottomToolStripPanel.SuspendLayout()
        Me._ToolStripContainer.ContentPanel.SuspendLayout()
        Me._ToolStripContainer.TopToolStripPanel.SuspendLayout()
        Me._ToolStripContainer.SuspendLayout()
        Me._BottomToolStrip.SuspendLayout()
        Me._TopToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_EntityCollectionGrid
        '
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGreen
        Me._EntityCollectionGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me._EntityCollectionGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._EntityCollectionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me._EntityCollectionGrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me._EntityCollectionGrid.IgnoreEditingErrors = False
        Me._EntityCollectionGrid.Location = New System.Drawing.Point(0, 0)
        Me._EntityCollectionGrid.MultiSelect = False
        Me._EntityCollectionGrid.Name = "_EntityCollectionGrid"
        Me._EntityCollectionGrid.ShowRowNumbers = False
        Me._EntityCollectionGrid.Size = New System.Drawing.Size(721, 467)
        Me._EntityCollectionGrid.TabIndex = 4
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_ToolStripContainer
        '
        '
        '_ToolStripContainer.BottomToolStripPanel
        '
        Me._ToolStripContainer.BottomToolStripPanel.Controls.Add(Me._BottomToolStrip)
        '
        '_ToolStripContainer.ContentPanel
        '
        Me._ToolStripContainer.ContentPanel.Controls.Add(Me._EntityCollectionGrid)
        Me._ToolStripContainer.ContentPanel.Size = New System.Drawing.Size(721, 467)
        Me._ToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ToolStripContainer.Location = New System.Drawing.Point(0, 0)
        Me._ToolStripContainer.Name = "_ToolStripContainer"
        Me._ToolStripContainer.Size = New System.Drawing.Size(721, 531)
        Me._ToolStripContainer.TabIndex = 5
        Me._ToolStripContainer.Text = "Tool Strip Container"
        '
        '_ToolStripContainer.TopToolStripPanel
        '
        Me._ToolStripContainer.TopToolStripPanel.Controls.Add(Me._TopToolStrip)
        '
        '_BottomToolStrip
        '
        Me._BottomToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._BottomToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._RefreshButton, Me._SaveButtonSeparator, Me._SaveButton, Me._PrintButtonSeparator, Me._PrintButton, Me._ExportButtonSeparator, Me._ExportButton, Me._ActionsButtonSeparator, Me._ActionsButton, Me._BottomProgressBarSeparator, Me._BottomProgressBar, Me._BottomStatusLabelSeparator, Me._BottomStatusLabel})
        Me._BottomToolStrip.Location = New System.Drawing.Point(3, 0)
        Me._BottomToolStrip.Name = "_BottomToolStrip"
        Me._BottomToolStrip.Size = New System.Drawing.Size(415, 39)
        Me._BottomToolStrip.TabIndex = 0
        '
        '_SaveButtonSeparator
        '
        Me._SaveButtonSeparator.Name = "_SaveButtonSeparator"
        Me._SaveButtonSeparator.Size = New System.Drawing.Size(6, 39)
        '
        '_PrintButtonSeparator
        '
        Me._PrintButtonSeparator.Name = "_PrintButtonSeparator"
        Me._PrintButtonSeparator.Size = New System.Drawing.Size(6, 39)
        '
        '_ActionButtonSeparator
        '
        Me._ActionsButtonSeparator.Name = "_ActionButtonSeparator"
        Me._ActionsButtonSeparator.Size = New System.Drawing.Size(6, 39)
        '
        '_BottomProgressBarSeparator
        '
        Me._BottomProgressBarSeparator.Name = "_BottomProgressBarSeparator"
        Me._BottomProgressBarSeparator.Size = New System.Drawing.Size(6, 39)
        '
        '_BottomProgressBar
        '
        Me._BottomProgressBar.Name = "_BottomProgressBar"
        Me._BottomProgressBar.Size = New System.Drawing.Size(100, 36)
        '
        '_BottomStatusLabelSeparator
        '
        Me._BottomStatusLabelSeparator.Name = "_BottomStatusLabelSeparator"
        Me._BottomStatusLabelSeparator.Size = New System.Drawing.Size(6, 39)
        '
        '_BottomStatusLabel
        '
        Me._BottomStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._BottomStatusLabel.Name = "_BottomStatusLabel"
        Me._BottomStatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._BottomStatusLabel.Size = New System.Drawing.Size(54, 36)
        Me._BottomStatusLabel.Text = "<status>"
        '
        '_TopToolStrip
        '
        Me._TopToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._TopToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TitleLabel})
        Me._TopToolStrip.Location = New System.Drawing.Point(3, 0)
        Me._TopToolStrip.Name = "_TopToolStrip"
        Me._TopToolStrip.Size = New System.Drawing.Size(162, 25)
        Me._TopToolStrip.TabIndex = 0
        '
        '_TitleLabel
        '
        Me._TitleLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TitleLabel.Name = "_TitleLabel"
        Me._TitleLabel.Size = New System.Drawing.Size(150, 22)
        Me._TitleLabel.Text = "Entity Collection Name"
        '
        '_ExportButtonSeparator
        '
        Me._ExportButtonSeparator.Name = "_ExportButtonSeparator"
        Me._ExportButtonSeparator.Size = New System.Drawing.Size(6, 39)
        '
        '_RefreshButton
        '
        Me._RefreshButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._RefreshButton.Image = Global.isr.Data.LLBLGen.My.Resources.Resources.View_refresh_4
        Me._RefreshButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._RefreshButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._RefreshButton.Name = "_RefreshButton"
        Me._RefreshButton.Size = New System.Drawing.Size(36, 36)
        Me._RefreshButton.Text = "REFRESH"
        '
        '_SaveButton
        '
        Me._SaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SaveButton.Image = Global.isr.Data.LLBLGen.My.Resources.Resources.Document_save_3
        Me._SaveButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._SaveButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SaveButton.Name = "_SaveButton"
        Me._SaveButton.Size = New System.Drawing.Size(36, 36)
        Me._SaveButton.Text = "SAVE"
        '
        '_PrintButton
        '
        Me._PrintButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._PrintButton.Image = Global.isr.Data.LLBLGen.My.Resources.Resources.Document_print_3
        Me._PrintButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._PrintButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._PrintButton.Name = "_PrintButton"
        Me._PrintButton.Size = New System.Drawing.Size(36, 36)
        Me._PrintButton.Text = "PRINT"
        '
        '_ExportButton
        '
        Me._ExportButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ExportButton.Image = Global.isr.Data.LLBLGen.My.Resources.Resources.Document_export_4
        Me._ExportButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ExportButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ExportButton.Name = "_ExportButton"
        Me._ExportButton.Size = New System.Drawing.Size(36, 36)
        Me._ExportButton.Text = "EXPORT"
        '
        '_ActionsButton
        '
        Me._ActionsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ActionsButton.Image = Global.isr.Data.LLBLGen.My.Resources.Resources.System_run_5
        Me._ActionsButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ActionsButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ActionsButton.Name = "_ActionsButton"
        Me._ActionsButton.Size = New System.Drawing.Size(36, 36)
        Me._ActionsButton.Text = "ACTION"
        '
        'EntityCollectionManager
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me._ToolStripContainer)
        Me.Name = "EntityCollectionManager"
        Me.Size = New System.Drawing.Size(721, 531)
        CType(Me._EntityCollectionGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ToolStripContainer.BottomToolStripPanel.ResumeLayout(False)
        Me._ToolStripContainer.BottomToolStripPanel.PerformLayout()
        Me._ToolStripContainer.ContentPanel.ResumeLayout(False)
        Me._ToolStripContainer.TopToolStripPanel.ResumeLayout(False)
        Me._ToolStripContainer.TopToolStripPanel.PerformLayout()
        Me._ToolStripContainer.ResumeLayout(False)
        Me._ToolStripContainer.PerformLayout()
        Me._BottomToolStrip.ResumeLayout(False)
        Me._BottomToolStrip.PerformLayout()
        Me._TopToolStrip.ResumeLayout(False)
        Me._TopToolStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _EntityCollectionGrid As CollectionDataGridView
    Private WithEvents _ToolStripContainer As System.Windows.Forms.ToolStripContainer
    Private WithEvents _BottomToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _RefreshButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _TopToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _TitleLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _SaveButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _SaveButtonSeparator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _PrintButtonSeparator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _PrintButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _ActionsButtonSeparator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _ActionsButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _BottomStatusLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _BottomStatusLabelSeparator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _BottomProgressBarSeparator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _BottomProgressBar As System.Windows.Forms.ToolStripProgressBar
    Private WithEvents _ExportButtonSeparator As System.Windows.Forms.ToolStripSeparator
    Private WithEvents _ExportButton As System.Windows.Forms.ToolStripButton

End Class
