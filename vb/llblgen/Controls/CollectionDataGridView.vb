Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Data.LLBLGen.ExceptionExtensions
''' <summary> Extends the data grid view to save LLBLGEN Collections. </summary>
''' <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-08-07, 2.2.4602. </para></remarks>
Public Class CollectionDataGridView
    Inherits DataGridView

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="CollectionDataGridView" /> class. </summary>
    Public Sub New()

        MyBase.New()
        Me.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.LightGreen
        Me.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.None
        Me.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.EnableHeadersVisualStyles = True
        Me.MultiSelect = False
        Me.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources when called from the
    '''                          runtime finalize. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveContentsChangedEvent(Me.ContentsChangedEvent)
                Me.RemoveDeleteRequestedEvent(Me.DeleteRequestedEvent)
                Me.RemoveEditableChangedEvent(Me.EditableChangedEvent)
                Me.RemoveRefreshedEvent(Me.RefreshedEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub
#End Region

#Region " CELL FORMATTING "

#If DoesNotWork Then
    ''' <summary>
    ''' Work around to a problem with the data grid view failure to handle the format provider.
    ''' http://www.CodeProject.com/KB/cs/CUSTSTRFORMAT.aspx?df=100+forumid=36436+exp=0+select=1661578#xx1661578xx 
    ''' </summary>
    ''' <remarks>Removed: 4/25: THis fails with the Core EngineeringFormatProvider.</remarks>
    Protected Overrides Sub OnCellFormatting(ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs)
        If e Is Nothing Then Return
        Dim col As DataGridViewColumn = Me.Columns(e.ColumnIndex)
        If TypeOf col.DefaultCellStyle.FormatProvider Is ICustomFormatter Then
            Dim originalData As Object = e.Value
            e.Value = String.Format(col.DefaultCellStyle.FormatProvider, col.DefaultCellStyle.Format, originalData)
        Else
            MyBase.OnCellFormatting(e)
        End If
    End Sub
#End If

    ''' <summary> Work around to a problem with the data grid view failure to handle the format
    ''' provider. http://stackoverflow.com/questions/3627922/format-time-span-in-datagridview-column. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnCellFormatting(ByVal e As DataGridViewCellFormattingEventArgs)
        If e Is Nothing Then
            MyBase.OnCellFormatting(e)
        Else
            Dim formatter As ICustomFormatter = TryCast(e.CellStyle.FormatProvider, ICustomFormatter)
            If formatter Is Nothing Then
                MyBase.OnCellFormatting(e)
            Else
                e.Value = formatter.Format(e.CellStyle.Format, e.Value, e.CellStyle.FormatProvider)
                e.FormattingApplied = True
            End If
        End If
    End Sub

#End Region

#Region " DATA MANAGEMENT "

    Private _BindingSource As BindingSource

    ''' <summary> Gets the binding source. </summary>
    ''' <value> The binding source. </value>
    Public ReadOnly Property BindingSource As BindingSource
        Get
            Return Me._bindingSource
        End Get
    End Property

    ''' <summary> Gets or sets the data source that the
    ''' <see cref="T:System.Windows.Forms.DataGridView" /> is displaying data for. </summary>
    ''' <value> The object that contains data for the
    ''' <see cref="T:System.Windows.Forms.DataGridView" /> to display. </value>
    Public Overloads Property DataSource As Object
        Get
            Return Me._BindingSource?.DataSource
        End Get
        Set(value As Object)
            Me.SuspendLayout()
            Me._BindingSource = New BindingSource With {.DataSource = value}
            MyBase.DataSource = Me._BindingSource
            Me.RowsAddedCount = 0
            Me.RowsRemovedCount = 0
            If Not Me.DesignMode Then
                Me._Collection = TryCast(value, SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection)
                If Me._Collection IsNot Nothing Then
                    Me._RemovedEntities = Me._Collection.RemovedEntitiesTracker
                End If
                If Me.AutoGenerateColumns Then
                    Do Until Me.Columns IsNot Nothing AndAlso Me.Columns.Count > 0
                        Windows.Forms.Application.DoEvents()
                    Loop
                End If
                Me.OnContentsChanged(System.EventArgs.Empty)
            End If
            Me.ResumeLayout(False)
        End Set
    End Property

#End Region

#Region " COLLECTION MANAGEMENT "

    ''' <summary> Occurs when the
    ''' <see cref="E:System.Windows.Forms.DataGridView.AllowUserToAddRowsChanged" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnAllowUserToAddRowsChanged(e As System.EventArgs)
        MyBase.OnAllowUserToAddRowsChanged(e)
        Me.UpdateCollectionBinding()
    End Sub

    ''' <summary> Occurs when the
    ''' <see cref="E:System.Windows.Forms.DataGridView.AllowUserToDeleteRowsChanged" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnAllowUserToDeleteRowsChanged(e As System.EventArgs)
        MyBase.OnAllowUserToDeleteRowsChanged(e)
        ' The user can delete rows if the SelectionMode property is set to FullRowSelect 
        ' or RowHeaderSelect and the MultiSelect property is set to true. 
        ' If the DataGridView is bound to data, the BindingList.AllowRemove property of the data source must also be set to true.
        If Me.AllowUserToDeleteRows Then
            Me.MultiSelect = True
            Me.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        Else
            Me.MultiSelect = False
            Me.SelectionMode = DataGridViewSelectionMode.CellSelect
        End If
        Me.UpdateCollectionBinding()
    End Sub

    ''' <summary> Occurs when the <see cref="E:System.Windows.Forms.DataGridView.ReadOnlyChanged" />
    ''' event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnReadOnlyChanged(e As System.EventArgs)
        MyBase.OnReadOnlyChanged(e)
        Me.IsEditable = Not Me.ReadOnly
    End Sub

    ''' <summary> Occurs when [editable changed]. </summary>
    Public Event EditableChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes editable changed event. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub RemoveEditableChangedEvent(ByVal value As EventHandler(Of EventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList

            Try
                RemoveHandler Me.EditableChanged, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Occurs when the <see cref="E:EditableChanged" /> event. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Overridable Sub OnEditableChanged(e As System.EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.EditableChangedEvent
        evt?.Invoke(Me, e)
        Me.UpdateCollectionBinding()
        Me.EditMode = If(Me.IsEditable, DataGridViewEditMode.EditOnKeystroke, DataGridViewEditMode.EditProgrammatically)
        Me.RowHeadersVisible = Me.IsEditable
        Me.ShowEditingIcon = Me.IsEditable
    End Sub

    Private _IsEditable As Boolean

    ''' <summary> Allows editing. </summary>
    ''' <value> The is editable. </value>
    <Category("Behavior"), Description("Allows editing."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property IsEditable As Boolean
        Get
            Return Me._IsEditable
        End Get
        Set(value As Boolean)
            Me._IsEditable = value
            Me.OnEditableChanged(System.EventArgs.Empty)
        End Set
    End Property

    ''' <summary> Updates the collection binding. Sets editing, deletion and addition capabilities. </summary>
    Private Sub UpdateCollectionBinding()
        If Not Me.DesignMode AndAlso Me._Collection IsNot Nothing Then
            Me._Collection.AllowRemove = Me.AllowUserToDeleteRows
            Me._Collection.AllowNew = Me.AllowUserToAddRows
            Me._Collection.AllowEdit = Not Me.ReadOnly
        End If
    End Sub

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Collection As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the collection. </summary>
    ''' <returns> The collection. </returns>
    Public Function Collection() As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection
        Return Me._Collection
    End Function

    ''' <summary> Assigns collection. </summary>
    ''' <remarks> Using a collection property, even if hidden, causes the container form to fail
    ''' opening in design mode. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub AssignCollection(ByVal value As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection)
        Me._Collection = value
        Me.UpdateCollectionBinding()
    End Sub

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _RemovedEntities As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection
#Enable Warning IDE1006 ' Naming Styles
    ''' <summary> Gets the removed entities. </summary>
    ''' <returns> The removed entities. </returns>
    Public Function RemovedEntities() As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection
        Return Me._RemovedEntities
    End Function

    ''' <summary> Assigns the removed entities and tracker. </summary>
    ''' <param name="value"> The collection holding the removed entities. </param>
    Public Sub AssignRemovedEntities(ByVal value As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection)
        Me._RemovedEntities = value
        If value IsNot Nothing Then
            Me._Collection.RemovedEntitiesTracker = Me._RemovedEntities
        End If
    End Sub

    ''' <summary> Gets a value indicating whether this instance is dirty. </summary>
    ''' <value> The is dirty. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property IsDirty As Boolean
        Get
            Return (Me.RowsAddedCount > 0) OrElse
                   (Me.RowsRemovedCount > 0) OrElse
                   (Me._Collection IsNot Nothing AndAlso
                        (Me._Collection.ContainsDirtyContents OrElse
                           (Me._Collection.RemovedEntitiesTracker IsNot Nothing AndAlso
                                Me._Collection.RemovedEntitiesTracker.Count > 0)))
        End Get
    End Property

    ''' <summary> Handles the ListChanged eventS of the edited or deleted collection. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.ComponentModel.ListChangedEventArgs" /> instance
    ''' containing the event data. </param>
    Private Sub HandlesListChanged(sender As Object, e As System.ComponentModel.ListChangedEventArgs) Handles _Collection.ListChanged,
                                                                                                       _RemovedEntities.ListChanged
        Me.OnContentsChanged(e)
    End Sub

#End Region

#Region " EDIT MANAGEMENT "

    ''' <summary> Occurs when a Print display is requested. </summary>
    Public Event DeleteRequested As EventHandler(Of System.Windows.Forms.DataGridViewRowCancelEventArgs)


    ''' <summary> Removes delete requested event. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub RemoveDeleteRequestedEvent(ByVal value As EventHandler(Of System.Windows.Forms.DataGridViewRowCancelEventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.DeleteRequested, CType(d, EventHandler(Of System.Windows.Forms.DataGridViewRowCancelEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Print requested. </summary>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub OnDeleteRequested(ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs)
        Dim evt As EventHandler(Of System.Windows.Forms.DataGridViewRowCancelEventArgs) = Me.DeleteRequestedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Handles the UserDeletingRow event of the EntityCollectionGrid control. Allows the
    ''' user to cancel the deletion. </summary>
    ''' <param name="e"> The <see cref="System.Windows.Forms.DataGridViewRowCancelEventArgs" />
    ''' instance containing the event data. </param>
    Protected Overrides Sub OnUserDeletingRow(e As System.Windows.Forms.DataGridViewRowCancelEventArgs)
        If e IsNot Nothing AndAlso (Not e.Row.IsNewRow) Then
            Me.OnDeleteRequested(e)
            If Not e.Cancel Then
                Dim response As DialogResult =
                    System.Windows.Forms.MessageBox.Show("Are you sure you want to delete this row?", "Delete row?",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2,
                                    MessageBoxOptions.DefaultDesktopOnly)
                If (response = DialogResult.No) Then
                    e.Cancel = True
                End If
            End If
        End If
    End Sub

    ''' <summary> Gets or sets the row added count. </summary>
    ''' <value> The row added count. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property RowsAddedCount As Integer

    ''' <summary> Gets or sets the row deleted count. </summary>
    ''' <value> The row deleted count. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property RowsRemovedCount As Integer

    ''' <summary> Saves the changes. Returns the total count of the items saved and removed. </summary>
    ''' <returns> A list of. </returns>
    Public Function SaveChanges() As Integer
        Dim itemsRemoved As Integer = 0
        If Me._Collection.RemovedEntitiesTracker IsNot Nothing AndAlso Me._Collection.RemovedEntitiesTracker.Count > 0 Then
            itemsRemoved = Me._Collection.RemovedEntitiesTracker.DeleteMulti()
        End If
        Dim itemsSaved As Integer = 0
        If Me._Collection.ContainsDirtyContents Then
            itemsSaved = Me._Collection.SaveMulti()
        End If
        Return itemsSaved + itemsRemoved
    End Function

#End Region

#Region " DISPLAY ORDER MANAGEMENT "

    Private _ColumnDisplayOrder As List(Of KeyValuePair(Of String, Integer))

    ''' <summary> Saves the column order. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="order"> The order. </param>
    Public Sub SaveColumnDisplayOrder(ByVal order As List(Of KeyValuePair(Of String, Integer)))
        If order Is Nothing Then Throw New ArgumentNullException(NameOf(order))
        ' create a list of columns ordered by column index.
        Dim sl As New SortedList(Of Integer, String)
        For Each kvp As KeyValuePair(Of String, Integer) In order
            sl.Add(kvp.Value, kvp.Key)
        Next
        sl.Reverse()
        Me._ColumnDisplayOrder = New List(Of KeyValuePair(Of String, Integer))
        For Each kvp As KeyValuePair(Of Integer, String) In sl
            Me._ColumnDisplayOrder.Add(New KeyValuePair(Of String, Integer)(kvp.Value, kvp.Key))
        Next

    End Sub

    ''' <summary> Saves the column order. </summary>
    Public Sub SaveColumnDisplayOrder()
        ' create a list of columns ordered by column index. 
        Dim sl As New SortedList(Of Integer, String)
        For Each col As DataGridViewColumn In Me.Columns
            If col.Visible Then sl.Add(col.DisplayIndex, col.Name)
        Next
        sl.Reverse()
        Me._ColumnDisplayOrder = New List(Of KeyValuePair(Of String, Integer))
        For Each kvp As KeyValuePair(Of Integer, String) In sl
            Me._ColumnDisplayOrder.Add(New KeyValuePair(Of String, Integer)(kvp.Value, kvp.Key))
        Next
    End Sub

    ''' <summary> Determines whether the <see cref="datagridview">grid</see> column order mismatches
    ''' the saved order. </summary>
    ''' <returns> <c>True</c> if column order mismatches; otherwise, <c>False</c>. </returns>
    Public Function IsColumnDisplayOrderMismatch() As Boolean
        Dim columnOrderMismatch As Boolean = False
        If Me._ColumnDisplayOrder Is Nothing Then
            Return columnOrderMismatch
        Else
            For Each col As DataGridViewColumn In Me.Columns
                If col.Visible Then
                    If Not Me._ColumnDisplayOrder.Contains(New KeyValuePair(Of String, Integer)(col.Name, col.DisplayIndex)) Then
                        columnOrderMismatch = True
                        Exit For
                    End If
                End If
            Next
        End If
        Return columnOrderMismatch
    End Function

    ''' <summary> Determines whether [is save column display order required]. </summary>
    ''' <returns> <c>True</c> if [is save column display order required]; otherwise, <c>False</c>. </returns>
    Public Function IsSaveColumnDisplayOrderRequired() As Boolean
        Return Me._ColumnDisplayOrder Is Nothing OrElse Me._ColumnDisplayOrder.Count = 0
    End Function

    ''' <summary> Updates the column order. </summary>
    Public Sub UpdateColumnDisplayOrder()
        If Me._ColumnDisplayOrder Is Nothing Then
            Me.Enabled = False
            Me.Visible = False
            Me.Invalidate()
            Me.Visible = True
            Me.Enabled = True
        ElseIf Me.IsColumnDisplayOrderMismatch Then
            For Each kvp As KeyValuePair(Of String, Integer) In Me._ColumnDisplayOrder
                Me.Columns(kvp.Key).DisplayIndex = kvp.Value
            Next
        End If
    End Sub

#End Region

#Region " DATA ERROR "

    ''' <summary> Ignores editing errors. </summary>
    ''' <value> The ignore editing errors. </value>
    <Category("Behavior"), Description("Ignores editing errors."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(True)>
    Public Property IgnoreEditingErrors As Boolean

    ''' <summary> Gets or sets a value indicating whether to suppress data error. </summary>
    ''' <value> <c>True</c> if suppressing data error; otherwise, <c>False</c>. </value>
    <Category("Behavior"), Description("Suppresses all data errors."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property SuppressDataError As Boolean

    ''' <summary> Ignores the data error. </summary>
    ''' <remarks> Uses the collection to determine if the collection is in edit mode. </remarks>
    ''' <param name="sender">     The sender. </param>
    ''' <param name="e">          The <see cref="System.Windows.Forms.DataGridViewDataErrorEventArgs"/>
    ''' instance containing the event data. </param>
    ''' <param name="collection"> The collection. </param>
    ''' <returns> <c>True</c> if editing error can be ignored, <c>False</c> otherwise. </returns>
    Public Shared Function IgnoreDataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs,
                                           ByVal collection As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection) As Boolean

        If e Is Nothing OrElse sender Is Nothing Then
            Return True
        Else
            Dim grid As DataGridView = TryCast(sender, DataGridView)
            If grid Is Nothing Then
                Return True
            ElseIf collection IsNot Nothing AndAlso
                (collection.ContainsDirtyContents OrElse
                 (collection.RemovedEntitiesTracker IsNot Nothing AndAlso collection.RemovedEntitiesTracker.Count > 0)) Then
                Return True
            ElseIf grid.CurrentCell Is Nothing OrElse grid.CurrentRow Is Nothing Then
                Return True
            ElseIf grid.CurrentCell.IsInEditMode OrElse grid.CurrentRow.IsNewRow OrElse grid.IsCurrentCellInEditMode OrElse grid.IsCurrentRowDirty Then
                Return True
            End If
        End If
        Return False

    End Function

    ''' <summary> Builds the data error. </summary>
    ''' <param name="grid"> The grid. </param>
    ''' <param name="e">    The <see cref="System.Windows.Forms.DataGridViewDataErrorEventArgs"/>
    ''' instance containing the event data. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function BuildDataError(ByVal grid As DataGridView, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) As String
        ' prevent error reporting when adding a new row or editing a cell
        If grid Is Nothing OrElse e Is Nothing OrElse e.Exception Is Nothing OrElse
            grid.CurrentCell Is Nothing OrElse grid.CurrentRow Is Nothing Then
            Return String.Empty
        Else
            Dim cellValue As String = "nothing"
            If grid.CurrentCell.Value IsNot Nothing Then
                cellValue = grid.CurrentCell.Value.ToString
            End If
            Return $"Data error occurred at Grid {grid.Name}(R{e.RowIndex},C{e.ColumnIndex}):{grid.Columns(e.ColumnIndex).Name}. Cell value is '{cellValue}';. {e.Exception.ToFullBlownString}"
        End If
    End Function

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.DataGridView.DataError" /> event.
    ''' Ignores error if editing. </summary>
    ''' <remarks> Allows suppression of data error. This became necessary as we are unable to prevent
    ''' data error when changing the data source of a data grid with combo boxes. </remarks>
    ''' <param name="displayErrorDialogIfNoHandler"> true to display an error dialog box if there is
    ''' no handler for the <see cref="E:System.Windows.Forms.DataGridView.DataError" /> event. </param>
    ''' <param name="e">                             A <see cref="T:System.Windows.Forms.DataGridViewDataErrorEventArgs" /> 
    '''                                              that contains the event data. </param>
    Protected Overloads Overrides Sub OnDataError(ByVal displayErrorDialogIfNoHandler As Boolean,
                                                  ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs)
        If Me.IgnoreEditingErrors Then
            If Me.IsCurrentRowDirty Then Return
            If Me.IsDirty Then Return
            If Me.CurrentRow IsNot Nothing AndAlso Me.CurrentRow.IsNewRow Then Return
            If Me.IsCurrentCellInEditMode Then Return
        End If
        If Not Me.SuppressDataError Then
            MyBase.OnDataError(displayErrorDialogIfNoHandler, e)
        End If
    End Sub

#End Region

#Region " GRID EVENTS "

    ''' <summary> Occurs when a refresh display is requested. </summary>
    Public Event Refreshed As EventHandler(Of System.EventArgs)

    ''' <summary> Removes refreshed event. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub RemoveRefreshedEvent(ByVal value As EventHandler(Of EventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.Refreshed, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the <see cref="E:ContentsChanged" /> event. </summary>
    Public Sub OnRefreshed()
        Me.OnRefreshed(System.EventArgs.Empty)
    End Sub

    ''' <summary> Raises the <see cref="E:ContentsChanged" /> event. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub OnRefreshed(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.RefreshedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Occurs when after contents changed. </summary>
    Public Event ContentsChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes contents changed event. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub RemoveContentsChangedEvent(ByVal value As EventHandler(Of EventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.ContentsChanged, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the <see cref="E:ContentsChanged" /> event. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub OnContentsChanged(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.ContentsChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Occurs with the <see cref="E:System.Windows.Forms.DataGridView.UserAddedRow" />
    ''' event. Updates the number of added rows. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.DataGridViewRowEventArgs" /> that
    ''' contains the event data. </param>
    Protected Overrides Sub OnUserAddedRow(e As System.Windows.Forms.DataGridViewRowEventArgs)
        MyBase.OnUserAddedRow(e)
        If Me._Collection Is Nothing OrElse Me._Collection.Count = 0 Then
            Me.RowsAddedCount += 1
        End If
    End Sub

    ''' <summary> Occurs with the <see cref="E:System.Windows.Forms.DataGridView.UserDeletedRow" />
    ''' event. Updates the number of removed rows. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.DataGridViewRowEventArgs" /> that
    ''' contains the event data. </param>
    Protected Overrides Sub OnUserDeletedRow(e As System.Windows.Forms.DataGridViewRowEventArgs)
        MyBase.OnUserDeletedRow(e)
        If Me._Collection Is Nothing OrElse Me._Collection.Count = 0 Then
            Me.RowsRemovedCount += 1
        End If
    End Sub

    ''' <summary> Gets or sets the sentinel indicating if row numbers will show. </summary>
    ''' <value> The show row number. </value>
    ''' <remarks> Requires </remarks>
    Public Property ShowRowNumbers As Boolean

    ''' <summary> Collection data grid view row post paint. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Data grid view row post paint event information. </param>
    Private Sub CollectionDataGridView_RowPostPaint(sender As Object, e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs) Handles Me.RowPostPaint
        If Me.ShowRowNumbers AndAlso Me.RowHeadersVisible Then
            Using b As New Drawing.SolidBrush(Me.RowHeadersDefaultCellStyle.ForeColor)
                e.Graphics.DrawString((e.RowIndex + 1).ToString, e.InheritedRowStyle.Font,
                                      b, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 4)
            End Using
        End If
    End Sub

#End Region

End Class
