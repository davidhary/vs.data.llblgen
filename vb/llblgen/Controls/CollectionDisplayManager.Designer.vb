﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class CollectionDisplayManager

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._ControlsLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._printButton = New System.Windows.Forms.Button()
        Me._SaveButton = New System.Windows.Forms.Button()
        Me._Layout.SuspendLayout()
        Me._ControlsLayout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.368932!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 95.63107!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24.0!))
        Me._Layout.Controls.Add(Me._ControlsLayout, 1, 2)
        Me._Layout.Location = New System.Drawing.Point(48, 53)
        Me._Layout.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 531.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me._Layout.Size = New System.Drawing.Size(745, 593)
        Me._Layout.TabIndex = 0
        '
        '_ControlsLayout
        '
        Me._ControlsLayout.ColumnCount = 5
        Me._ControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me._ControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me._ControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me._ControlsLayout.Controls.Add(Me._printButton, 3, 1)
        Me._ControlsLayout.Controls.Add(Me._SaveButton, 1, 1)
        Me._ControlsLayout.Dock = System.Windows.Forms.DockStyle.Top
        Me._ControlsLayout.Location = New System.Drawing.Point(34, 546)
        Me._ControlsLayout.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ControlsLayout.Name = "_ControlsLayout"
        Me._ControlsLayout.RowCount = 3
        Me._ControlsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._ControlsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ControlsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._ControlsLayout.Size = New System.Drawing.Size(683, 43)
        Me._ControlsLayout.TabIndex = 0
        '
        '_printButton
        '
        Me._printButton.AutoSize = True
        Me._printButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._printButton.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me._printButton.Location = New System.Drawing.Point(431, 7)
        Me._printButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._printButton.Name = "_printButton"
        Me._printButton.Padding = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me._printButton.Size = New System.Drawing.Size(69, 26)
        Me._printButton.TabIndex = 0
        Me._printButton.Text = "PRINT"
        Me._printButton.UseVisualStyleBackColor = True
        '
        '_SaveButton
        '
        Me._SaveButton.AutoSize = True
        Me._SaveButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SaveButton.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me._SaveButton.Location = New System.Drawing.Point(182, 6)
        Me._SaveButton.Name = "_SaveButton"
        Me._SaveButton.Padding = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me._SaveButton.Size = New System.Drawing.Size(64, 26)
        Me._SaveButton.TabIndex = 1
        Me._SaveButton.Text = "SAVE"
        Me._SaveButton.UseVisualStyleBackColor = True
        '
        'CollectionDisplayManager
        '
        Me.Controls.Add(Me._Layout)
        Me.Name = "CollectionDisplayManager"
        Me.Size = New System.Drawing.Size(894, 753)
        Me._Layout.ResumeLayout(False)
        Me._ControlsLayout.ResumeLayout(False)
        Me._ControlsLayout.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _ControlsLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _PrintButton As System.Windows.Forms.Button
    Private WithEvents _SaveButton As System.Windows.Forms.Button

End Class
