using System.Diagnostics;

namespace isr.Data.LLBLGen
{
    public partial class CollectionDisplayManager : Core.Forma.ModelViewBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2022-02-08. </remarks>
        public CollectionDisplayManager()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                          <c>False</c> to release only unmanaged resources when called from the
        ///                          runtime finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }
    }
}
