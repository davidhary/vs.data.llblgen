using System.Diagnostics;

namespace isr.Data.LLBLGen
{
    /// <summary>   Manager for collection displays. </summary>
    /// <remarks>   David, 2022-02-08. </remarks>
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class CollectionDisplayManager
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _ControlsLayout = new System.Windows.Forms.TableLayoutPanel();
            _PrintButton = new System.Windows.Forms.Button();
            _SaveButton = new System.Windows.Forms.Button();
            _Layout.SuspendLayout();
            _ControlsLayout.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.368932f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 95.63107f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24.0f));
            _Layout.Controls.Add(_ControlsLayout, 1, 2);
            _Layout.Location = new System.Drawing.Point(48, 53);
            _Layout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 531.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0f));
            _Layout.Size = new System.Drawing.Size(745, 593);
            _Layout.TabIndex = 0;
            // 
            // _ControlsLayout
            // 
            _ControlsLayout.ColumnCount = 5;
            _ControlsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333f));
            _ControlsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ControlsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333f));
            _ControlsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ControlsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333f));
            _ControlsLayout.Controls.Add(_PrintButton, 3, 1);
            _ControlsLayout.Controls.Add(_SaveButton, 1, 1);
            _ControlsLayout.Dock = System.Windows.Forms.DockStyle.Top;
            _ControlsLayout.Location = new System.Drawing.Point(34, 546);
            _ControlsLayout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _ControlsLayout.Name = "_ControlsLayout";
            _ControlsLayout.RowCount = 3;
            _ControlsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _ControlsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ControlsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _ControlsLayout.Size = new System.Drawing.Size(683, 43);
            _ControlsLayout.TabIndex = 0;
            // 
            // _printButton
            // 
            _PrintButton.AutoSize = true;
            _PrintButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _PrintButton.Font = new System.Drawing.Font(Font, System.Drawing.FontStyle.Bold);
            _PrintButton.Location = new System.Drawing.Point(431, 7);
            _PrintButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _PrintButton.Name = "_printButton";
            _PrintButton.Padding = new System.Windows.Forms.Padding(6, 0, 6, 0);
            _PrintButton.Size = new System.Drawing.Size(69, 26);
            _PrintButton.TabIndex = 0;
            _PrintButton.Text = "PRINT";
            _PrintButton.UseVisualStyleBackColor = true;
            // 
            // _SaveButton
            // 
            _SaveButton.AutoSize = true;
            _SaveButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _SaveButton.Font = new System.Drawing.Font(Font, System.Drawing.FontStyle.Bold);
            _SaveButton.Location = new System.Drawing.Point(182, 6);
            _SaveButton.Name = "_SaveButton";
            _SaveButton.Padding = new System.Windows.Forms.Padding(6, 0, 6, 0);
            _SaveButton.Size = new System.Drawing.Size(64, 26);
            _SaveButton.TabIndex = 1;
            _SaveButton.Text = "SAVE";
            _SaveButton.UseVisualStyleBackColor = true;
            // 
            // CollectionDisplayManager
            // 
            Controls.Add(_Layout);
            Name = "CollectionDisplayManager";
            Size = new System.Drawing.Size(894, 753);
            _Layout.ResumeLayout(false);
            _ControlsLayout.ResumeLayout(false);
            _ControlsLayout.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.TableLayoutPanel _ControlsLayout;
        private System.Windows.Forms.Button _PrintButton;
        private System.Windows.Forms.Button _SaveButton;
    }
}
