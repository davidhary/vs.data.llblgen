using System;
using System.Diagnostics;
using System.Windows.Forms;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Data.LLBLGen
{
    [DesignerGenerated()]
    public partial class EntityCollectionManager
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            _EntityCollectionGrid = new CollectionDataGridView();
            _ErrorProvider = new System.Windows.Forms.ErrorProvider(components);
            _ToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            _BottomToolStrip = new System.Windows.Forms.ToolStrip();
            _SaveButtonSeparator = new System.Windows.Forms.ToolStripSeparator();
            _PrintButtonSeparator = new System.Windows.Forms.ToolStripSeparator();
            _ActionsButtonSeparator = new System.Windows.Forms.ToolStripSeparator();
            _BottomProgressBarSeparator = new System.Windows.Forms.ToolStripSeparator();
            _BottomProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            _BottomStatusLabelSeparator = new System.Windows.Forms.ToolStripSeparator();
            _BottomStatusLabel = new System.Windows.Forms.ToolStripLabel();
            _TopToolStrip = new System.Windows.Forms.ToolStrip();
            _TitleLabel = new System.Windows.Forms.ToolStripLabel();
            _ExportButtonSeparator = new System.Windows.Forms.ToolStripSeparator();
            _RefreshButton = new System.Windows.Forms.ToolStripButton();
            _RefreshButton.Click += new EventHandler(RefreshButton_Click);
            _SaveButton = new System.Windows.Forms.ToolStripButton();
            _SaveButton.Click += new EventHandler(SaveButton_Click);
            _PrintButton = new System.Windows.Forms.ToolStripButton();
            _PrintButton.Click += new EventHandler(PrintButton_Click);
            _ExportButton = new System.Windows.Forms.ToolStripButton();
            _ExportButton.Click += new EventHandler(ExportButton_Click);
            _ActionsButton = new System.Windows.Forms.ToolStripButton();
            _ActionsButton.Click += new EventHandler(ActionsButton_Click);
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            _ToolStripContainer.BottomToolStripPanel.SuspendLayout();
            _ToolStripContainer.ContentPanel.SuspendLayout();
            _ToolStripContainer.TopToolStripPanel.SuspendLayout();
            _ToolStripContainer.SuspendLayout();
            _BottomToolStrip.SuspendLayout();
            _TopToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _EntityCollectionGrid
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGreen;
            _EntityCollectionGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            _EntityCollectionGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            _EntityCollectionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            _EntityCollectionGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            _EntityCollectionGrid.IgnoreEditingErrors = false;
            _EntityCollectionGrid.Location = new System.Drawing.Point(0, 0);
            _EntityCollectionGrid.MultiSelect = false;
            _EntityCollectionGrid.Name = "_EntityCollectionGrid";
            _EntityCollectionGrid.ShowRowNumbers = false;
            _EntityCollectionGrid.Size = new System.Drawing.Size(721, 467);
            _EntityCollectionGrid.TabIndex = 4;
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _ToolStripContainer
            // 
            // 
            // _ToolStripContainer.BottomToolStripPanel
            // 
            _ToolStripContainer.BottomToolStripPanel.Controls.Add(_BottomToolStrip);
            // 
            // _ToolStripContainer.ContentPanel
            // 
            _ToolStripContainer.ContentPanel.Controls.Add(_EntityCollectionGrid);
            _ToolStripContainer.ContentPanel.Size = new System.Drawing.Size(721, 467);
            _ToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            _ToolStripContainer.Location = new System.Drawing.Point(0, 0);
            _ToolStripContainer.Name = "_ToolStripContainer";
            _ToolStripContainer.Size = new System.Drawing.Size(721, 531);
            _ToolStripContainer.TabIndex = 5;
            _ToolStripContainer.Text = "Tool Strip Container";
            // 
            // _ToolStripContainer.TopToolStripPanel
            // 
            _ToolStripContainer.TopToolStripPanel.Controls.Add(_TopToolStrip);
            // 
            // _BottomToolStrip
            // 
            _BottomToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _BottomToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _RefreshButton, _SaveButtonSeparator, _SaveButton, _PrintButtonSeparator, _PrintButton, _ExportButtonSeparator, _ExportButton, _ActionsButtonSeparator, _ActionsButton, _BottomProgressBarSeparator, _BottomProgressBar, _BottomStatusLabelSeparator, _BottomStatusLabel });
            _BottomToolStrip.Location = new System.Drawing.Point(3, 0);
            _BottomToolStrip.Name = "_BottomToolStrip";
            _BottomToolStrip.Size = new System.Drawing.Size(415, 39);
            _BottomToolStrip.TabIndex = 0;
            // 
            // _SaveButtonSeparator
            // 
            _SaveButtonSeparator.Name = "_SaveButtonSeparator";
            _SaveButtonSeparator.Size = new System.Drawing.Size(6, 39);
            // 
            // _PrintButtonSeparator
            // 
            _PrintButtonSeparator.Name = "_PrintButtonSeparator";
            _PrintButtonSeparator.Size = new System.Drawing.Size(6, 39);
            // 
            // _ActionButtonSeparator
            // 
            _ActionsButtonSeparator.Name = "_ActionButtonSeparator";
            _ActionsButtonSeparator.Size = new System.Drawing.Size(6, 39);
            // 
            // _BottomProgressBarSeparator
            // 
            _BottomProgressBarSeparator.Name = "_BottomProgressBarSeparator";
            _BottomProgressBarSeparator.Size = new System.Drawing.Size(6, 39);
            // 
            // _BottomProgressBar
            // 
            _BottomProgressBar.Name = "_BottomProgressBar";
            _BottomProgressBar.Size = new System.Drawing.Size(100, 36);
            // 
            // _BottomStatusLabelSeparator
            // 
            _BottomStatusLabelSeparator.Name = "_BottomStatusLabelSeparator";
            _BottomStatusLabelSeparator.Size = new System.Drawing.Size(6, 39);
            // 
            // _BottomStatusLabel
            // 
            _BottomStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _BottomStatusLabel.Name = "_BottomStatusLabel";
            _BottomStatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            _BottomStatusLabel.Size = new System.Drawing.Size(54, 36);
            _BottomStatusLabel.Text = "<status>";
            // 
            // _TopToolStrip
            // 
            _TopToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _TopToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _TitleLabel });
            _TopToolStrip.Location = new System.Drawing.Point(3, 0);
            _TopToolStrip.Name = "_TopToolStrip";
            _TopToolStrip.Size = new System.Drawing.Size(162, 25);
            _TopToolStrip.TabIndex = 0;
            // 
            // _TitleLabel
            // 
            _TitleLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TitleLabel.Name = "_TitleLabel";
            _TitleLabel.Size = new System.Drawing.Size(150, 22);
            _TitleLabel.Text = "Entity Collection Name";
            // 
            // _ExportButtonSeparator
            // 
            _ExportButtonSeparator.Name = "_ExportButtonSeparator";
            _ExportButtonSeparator.Size = new System.Drawing.Size(6, 39);
            // 
            // _RefreshButton
            // 
            _RefreshButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            _RefreshButton.Image = isr.Data.LLBLGen.My.Resources.Resources.view_refresh_4;
            _RefreshButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            _RefreshButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _RefreshButton.Name = "_RefreshButton";
            _RefreshButton.Size = new System.Drawing.Size(36, 36);
            _RefreshButton.Text = "REFRESH";
            // 
            // _SaveButton
            // 
            _SaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            _SaveButton.Image = isr.Data.LLBLGen.My.Resources.Resources.document_save_3;
            _SaveButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            _SaveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SaveButton.Name = "_SaveButton";
            _SaveButton.Size = new System.Drawing.Size(36, 36);
            _SaveButton.Text = "SAVE";
            // 
            // _PrintButton
            // 
            _PrintButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            _PrintButton.Image = My.Resources.Resources.document_print_3;
            _PrintButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            _PrintButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _PrintButton.Name = "_PrintButton";
            _PrintButton.Size = new System.Drawing.Size(36, 36);
            _PrintButton.Text = "PRINT";
            // 
            // _ExportButton
            // 
            _ExportButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            _ExportButton.Image = My.Resources.Resources.document_export_4;
            _ExportButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            _ExportButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ExportButton.Name = "_ExportButton";
            _ExportButton.Size = new System.Drawing.Size(36, 36);
            _ExportButton.Text = "EXPORT";
            // 
            // _ActionsButton
            // 
            _ActionsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            _ActionsButton.Image = My.Resources.Resources.system_run_5;
            _ActionsButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            _ActionsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ActionsButton.Name = "_ActionsButton";
            _ActionsButton.Size = new System.Drawing.Size(36, 36);
            _ActionsButton.Text = "ACTION";
            // 
            // EntityCollectionManager
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            Controls.Add(_ToolStripContainer);
            Name = "EntityCollectionManager";
            Size = new System.Drawing.Size(721, 531);
            ((System.ComponentModel.ISupportInitialize)_EntityCollectionGrid).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            _ToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            _ToolStripContainer.BottomToolStripPanel.PerformLayout();
            _ToolStripContainer.ContentPanel.ResumeLayout(false);
            _ToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            _ToolStripContainer.TopToolStripPanel.PerformLayout();
            _ToolStripContainer.ResumeLayout(false);
            _ToolStripContainer.PerformLayout();
            _BottomToolStrip.ResumeLayout(false);
            _BottomToolStrip.PerformLayout();
            _TopToolStrip.ResumeLayout(false);
            _TopToolStrip.PerformLayout();
            VisibleChanged += new EventHandler(EntityCollectionManager_VisibleChanged);
            ResumeLayout(false);
        }

        private System.Windows.Forms.ErrorProvider _ErrorProvider;
        private System.Windows.Forms.ToolStripContainer _ToolStripContainer;
        private System.Windows.Forms.ToolStrip _BottomToolStrip;
        private System.Windows.Forms.ToolStripButton _RefreshButton;
        private System.Windows.Forms.ToolStrip _TopToolStrip;
        private System.Windows.Forms.ToolStripLabel _TitleLabel;
        private System.Windows.Forms.ToolStripButton _SaveButton;
        private System.Windows.Forms.ToolStripSeparator _SaveButtonSeparator;
        private System.Windows.Forms.ToolStripSeparator _PrintButtonSeparator;
        private System.Windows.Forms.ToolStripButton _PrintButton;
        private System.Windows.Forms.ToolStripSeparator _ActionsButtonSeparator;
        private System.Windows.Forms.ToolStripButton _ActionsButton;
        private System.Windows.Forms.ToolStripLabel _BottomStatusLabel;
        private System.Windows.Forms.ToolStripSeparator _BottomStatusLabelSeparator;
        private System.Windows.Forms.ToolStripSeparator _BottomProgressBarSeparator;
        private System.Windows.Forms.ToolStripSeparator _ExportButtonSeparator;
        private System.Windows.Forms.ToolStripButton _ExportButton;
        private CollectionDataGridView _EntityCollectionGrid;
        private ToolStripProgressBar _BottomProgressBar;
    }
}
