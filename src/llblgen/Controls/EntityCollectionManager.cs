using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Data.LLBLGen.ExceptionExtensions;

namespace isr.Data.LLBLGen
{
    /// <summary> Displays an entity collection. </summary>
    /// <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-01-27, 1.0.4409.x. </para></remarks>
    public partial class EntityCollectionManager : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTORS "

        /// <summary> Initializes a new instance of the <see cref="EntityCollectionManager" /> class. </summary>
        public EntityCollectionManager()
        {

            // This call is required by the designer.
            this.InitializeComponent();

            // hide controls
            this.ActionsEnabled = false;
            this.ActionsVisible = false;
            this.PrintingEnabled = false;
            this.PrintingVisible = false;
            this.ExportEnabled = false;
            this.ExportVisible = false;
            this.SavingEnabled = false;
            this.SaveVisible = false;
            this.RefreshingEnabled = false;
            this.RefreshingVisible = false;
            this.EntityCollectionTitle = string.Empty;
            this.BottomStatusMessage = string.Empty;
            this.BottomProgressBar.Visible = false;

            this.EntityCollectionGrid.AutoGenerateColumns = false;
            this.EntityCollectionGrid.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.LightGreen;
            this.EntityCollectionGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            this.EntityCollectionGrid.BorderStyle = BorderStyle.Fixed3D;
            this.EntityCollectionGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EntityCollectionGrid.EnableHeadersVisualStyles = true;
            this.EntityCollectionGrid.MultiSelect = false;
            this.EntityCollectionGrid.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised;
            this.EntityCollectionGrid.Refreshed += new EventHandler<EventArgs>( this.EntityCollectionGrid_Refreshed );
            this.EntityCollectionGrid.ContentsChanged += new EventHandler<EventArgs>( this.EntityCollectionGrid_ContentChanged );
            this.EntityCollectionGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler( this.EntityCollectionGrid_DataError );
            this.EntityCollectionGrid.DoubleClick += new EventHandler( this.EntityCollectionGrid_DoubleClick );
            this.EntityCollectionGrid.VisibleChanged += new EventHandler( this.EntityCollectionGrid_VisibleChanged );
            this.EntityCollectionGrid.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler( this.EntityCollectionGrid_UserDeletingRow );
            this.EntityCollectionGrid.DeleteRequested += new EventHandler<System.Windows.Forms.DataGridViewRowCancelEventArgs>( this.EntityCollectionGrid_DeletedRequested );
            this.EntityCollectionGrid.SelectionChanged += new EventHandler( this.EntityCollectionGrid_SelectionChanged );


            this.BottomStatusMessage = string.Empty;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases the
        /// managed resources.
        /// </summary>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveActionRequestedEvent( ActionRequested );
                    this.RemoveContentChangedEvent( ContentChanged );
                    this.RemoveDataErrorOccurredEvent( DataErrorOccurred );
                    this.RemoveDeleteRequestedEvent( DeleteRequested );
                    this.RemoveEntityCollectionSavedEvent( EntityCollectionSaved );
                    this.RemoveEntityCollectionUpdateRequestedEvent( EntityCollectionUpdateRequested );
                    this.RemoveExportRequestedEvent( ExportRequested );
                    this.RemoveGridVisibleChangedEvent( GridVisibleChanged );
                    this.RemoveRefreshRequestedEvent( RefreshRequested );
                    this.RemovePrintRequestedEvent( PrintRequested );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

#endregion

#region " TITLES "

        /// <summary> Gets or set the name of the information displayed in the table. </summary>
        /// <value> The entity collection title. </value>
        [Category( "Appearance" )]
        [Description( "The title of the data grid." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "ENTITY COLLECTION TITLE" )]
        public string EntityCollectionTitle
        {
            get => this._TitleLabel.Text;

            set {
                SafeTextSetter( this._TitleLabel, value );
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    this.EntityCollectionGrid.Name = this.Name;
                    this._TopToolStrip.Visible = false;
                }
                else
                {
                    this._TopToolStrip.Visible = true;
                    this.EntityCollectionGrid.Name = value;
                }
            }
        }

#endregion

#region " USER CONTROL "

        /// <summary> Called when visible. Updates the controls and fires the
        /// <see cref="GridVisibleChanged">Grid Visible Changed</see> event. </summary>
        public void OnVisible()
        {
            if ( this.EntityCollectionGrid.IsCurrentCellInEditMode )
                return;
            // showControls()
            var evt = GridVisibleChanged;
            evt?.Invoke( this, EventArgs.Empty );
        }

        /// <summary> Handles the VisibleChanged event of the EntityCollectionManager control. Updates the
        /// control if visible. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void EntityCollectionManager_VisibleChanged( object sender, EventArgs e )
        {
            if ( this.Visible )
            {
                this.OnVisible();
            }
        }

        /// <summary> Shows the controls. This hides the control per the relevant settings. </summary>
        private void ShowControls()
        {
            this.AddingRowsEnabled = this.AddingRowsEnabled;
            this.DeletingRowsEnabled = this.DeletingRowsEnabled;
            this.EditingEnabled = this.EditingEnabled;
            this.PrintingEnabled = this.EntityCollectionGrid.RowCount > 0;
            // refreshing will be enabled on the first display and stay so unless disabled externally.
            this.RefreshingEnabled = this.RefreshingEnabled || this.EntityCollectionGrid.RowCount > 0;
            this.ExportEnabled = this.ExportEnabled || this.EntityCollectionGrid.RowCount > 0;
            this.ActionsEnabled = this.ActionsVisible && this.EntityCollectionGrid.RowCount > 0;
            this.SavingEnabled = this.SavingEnabled || this.EditingEnabled && this.EntityCollectionGrid.IsDirty;
            bool anyVisible = false;
            foreach ( Control c in this._BottomToolStrip.Controls )
            {
                if ( c.Visible )
                {
                    anyVisible = true;
                    break;
                }
            }

            this._BottomToolStrip.Visible = anyVisible;
        }

#endregion

#region " CONTROLS "

        /// <summary> Enables or disables adding rows. </summary>
        /// <value> The adding rows enabled sentinel. </value>
        [Category( "Behavior" )]
        [Description( "Enables or disables adding rows." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool AddingRowsEnabled
        {
            get => this.EntityCollectionGrid.AllowUserToAddRows;

            set {
                this.EntityCollectionGrid.AllowUserToAddRows = value;
                this.EntityCollectionGrid.RowHeadersVisible = this.AddingRowsEnabled || this.EditingEnabled || this.DeletingRowsEnabled;
            }
        }

        /// <summary> Allows deleting rows. </summary>
        /// <value> The deleting rows enabled sentinel. </value>
        [Category( "Behavior" )]
        [Description( "Enables or disables deleting rows." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool DeletingRowsEnabled
        {
            get => this.EntityCollectionGrid.AllowUserToDeleteRows;

            set {
                this.EntityCollectionGrid.AllowUserToDeleteRows = value;
                this.EntityCollectionGrid.RowHeadersVisible = this.AddingRowsEnabled || this.EditingEnabled || this.DeletingRowsEnabled;
            }
        }

        /// <summary> Enables or disables editing. </summary>
        /// <value> The editing enabled sentinel. </value>
        [Category( "Behavior" )]
        [Description( "Enables or disables editing." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool EditingEnabled
        {
            get => this.EntityCollectionGrid.IsEditable;

            set {
                this.EntityCollectionGrid.IsEditable = value;
                this.EntityCollectionGrid.RowHeadersVisible = this.AddingRowsEnabled || this.EditingEnabled || this.DeletingRowsEnabled;
                this.SavingEnabled = value;
            }
        }

        /// <summary> Enables or disables Refreshing. </summary>
        /// <value> The Refreshing enabled sentinel. </value>
        /// <remarks> Refreshing is enabled on the first display in <see cref="ShowControls"/> and is latched until disabled externally. </remarks>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool RefreshingEnabled
        {
            get => this._RefreshButton.Enabled;

            set {
                if ( !value.Equals( this.RefreshingEnabled ) )
                {
                    this._RefreshButton.Enabled = value;
                }
            }
        }

        /// <summary> Shows or hides the refresh button. </summary>
        /// <value> The Refreshing Visible sentinel. </value>
        /// <remarks> Refreshing is Visible on the first display in <see cref="ShowControls"/> and is latched until disabled externally. </remarks>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool RefreshingVisible
        {
            get => this._RefreshButton.Visible;

            set {
                if ( !value.Equals( this.RefreshingVisible ) )
                {
                    this._RefreshButton.Visible = value;
                }
            }
        }

        /// <summary> Enables or disables Saving. </summary>
        /// <value> The Saving enabled sentinel. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool SavingEnabled
        {
            get => this._SaveButton.Enabled;

            set {
                if ( !value.Equals( this.SavingEnabled ) )
                {
                    this._SaveButton.Enabled = value;
                }
            }
        }

        /// <summary> Enables or disables printing. </summary>
        /// <value> The printing enabled sentinel. </value>
        [Category( "Behavior" )]
        [Description( "Enables or disables printing." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool PrintingEnabled
        {
            get => this._PrintButton.Enabled;

            set {
                if ( !value && !value.Equals( this.PrintingEnabled ) )
                {
                    // disabled
                    this._PrintButton.Enabled = value;
                    this._PrintButtonSeparator.Enabled = value;
                }
            }
        }

        /// <summary> Shows or hides printing. </summary>
        /// <value> The printing Visible sentinel. </value>
        [Category( "Behavior" )]
        [Description( "Shows or hides printing." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool PrintingVisible
        {
            get => this._PrintButton.Visible;

            set {
                if ( !value && !value.Equals( this.PrintingVisible ) )
                {
                    // disabled
                    this._PrintButton.Visible = value;
                    this._PrintButtonSeparator.Visible = value;
                }
            }
        }

        /// <summary> Updates the column order. </summary>
        public void UpdateColumnDisplayOrder()
        {
            this.EntityCollectionGrid.UpdateColumnDisplayOrder();
        }

        /// <summary> Gets the entity collection grid. </summary>
        /// <value> The entity collection grid. </value>
        public CollectionDataGridView EntityCollectionGrid => this._EntityCollectionGrid;

        /// <summary> Gets the bottom progress bar. </summary>
        /// <value> The bottom progress bar. </value>
        public ToolStripProgressBar BottomProgressBar => this._BottomProgressBar;

        #endregion

        #region " EVENTS "

        /// <summary> Occurs after the grid visibility changed. Used to update the columns. </summary>
        public event EventHandler<EventArgs> GridVisibleChanged;

        /// <summary> RemoveS grid visible changed event. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveGridVisibleChangedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    GridVisibleChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Occurs when a display update is requested. </summary>
        public event EventHandler<EventArgs> EntityCollectionUpdateRequested;

        /// <summary> Removes entity collection update requested event. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveEntityCollectionUpdateRequestedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    EntityCollectionUpdateRequested -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Entity collection update requested. </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void OnEntityCollectionUpdateRequested( EventArgs e )
        {
            var evt = EntityCollectionUpdateRequested;
            evt?.Invoke( this, e );
        }

        /// <summary> Occurs after data was saved. </summary>
        public event EventHandler<EventArgs> EntityCollectionSaved;

        /// <summary> Removes entity collection saved event. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveEntityCollectionSavedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    EntityCollectionSaved -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the <see cref="E:EntityCollectionChanged" /> event. </summary>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void OnEntityCollectionSaved( EventArgs e )
        {
            var evt = EntityCollectionSaved;
            evt?.Invoke( this, e );
        }

        /// <summary> Occurs when a refresh display is requested. </summary>
        public event EventHandler<EventArgs> RefreshRequested;

        /// <summary> Removes the refresh requested event described by value. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveRefreshRequestedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    RefreshRequested -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Refresh requested. </summary>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void OnRefreshRequested( EventArgs e )
        {
            var evt = RefreshRequested;
            evt?.Invoke( this, e );
        }

#endregion

#region " GRID EVENTS "

        /// <summary> Occurs when a refresh display is requested. </summary>
        public event EventHandler<EventArgs> ContentChanged;

        /// <summary> Removes content changed event 1. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveContentChangedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    ContentChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the <see cref="E:ContentsChanged" /> event. </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void OnContentsChanged( EventArgs e )
        {
            this.ActionsEnabled = this.EntityCollectionGrid.RowCount > 0;
            this.PrintingEnabled = this.EntityCollectionGrid.RowCount > 0;
            this.RefreshingEnabled = this.RefreshingEnabled || this.EntityCollectionGrid.RowCount > 0;
            this.SavingEnabled = (this.DeletingRowsEnabled || this.EditingEnabled) && this.EntityCollectionGrid.IsDirty;
            var evt = ContentChanged;
            evt?.Invoke( this, e );
        }

        /// <summary> Entity collection grid refreshed. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void EntityCollectionGrid_Refreshed( object sender, EventArgs e )
        {
            this.ShowControls();
        }

        /// <summary> Entity collection grid content changed. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void EntityCollectionGrid_ContentChanged( object sender, EventArgs e )
        {
            this.OnContentsChanged( e );
        }

        /// <summary> Occurs when [data error]. </summary>
        public event EventHandler<DataGridViewDataErrorEventArgs> DataErrorOccurred;

        /// <summary> Removes the data error occurred event described by value. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveDataErrorOccurredEvent( EventHandler<DataGridViewDataErrorEventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    DataErrorOccurred -= ( EventHandler<DataGridViewDataErrorEventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Handles the DataError event of the EntityCollectionGrid control. Reports the error to
        /// the parent control. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.Windows.Forms.DataGridViewDataErrorEventArgs" />
        /// instance containing the event data. </param>
        private void EntityCollectionGrid_DataError( object sender, DataGridViewDataErrorEventArgs e )
        {
            if ( DataErrorOccurred is object )
                DataErrorOccurred( this, e );
        }

        /// <summary> Entity collection grid double click. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void EntityCollectionGrid_DoubleClick( object sender, EventArgs e )
        {
            if ( this.EntityCollectionGrid.IsCurrentCellInEditMode )
                return;
            _ = this.RequestDisplayUpdate();
        }

        /// <summary> Handles the VisibleChanged event of the EntityCollectionGrid control. Needed to
        /// update the columns. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void EntityCollectionGrid_VisibleChanged( object sender, EventArgs e )
        {
            if ( this.EntityCollectionGrid.IsCurrentCellInEditMode )
                return;
            var evt = GridVisibleChanged;
            evt?.Invoke( this, e );
        }

        /// <summary> Occurs when a delete is requested. </summary>
        public event EventHandler<DataGridViewRowCancelEventArgs> DeleteRequested;

        /// <summary> Removes deleted requested event 1. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveDeleteRequestedEvent( EventHandler<DataGridViewRowCancelEventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    DeleteRequested -= ( EventHandler<DataGridViewRowCancelEventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the delete requested event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnDeleteRequested( DataGridViewRowCancelEventArgs e )
        {
            var evt = DeleteRequested;
            evt?.Invoke( this, e );
        }

        /// <summary> Entity collection grid user deleting row. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Data grid view row cancel event information. </param>
        private void EntityCollectionGrid_UserDeletingRow( object sender, DataGridViewRowCancelEventArgs e )
        {
            this.OnDeleteRequested( e );
        }

        /// <summary> Entity collection grid user deleting row. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Data grid view row cancel event information. </param>
        private void EntityCollectionGrid_DeletedRequested( object sender, DataGridViewRowCancelEventArgs e )
        {
            this.OnDeleteRequested( e );
        }

        /// <summary> Occurs when a selection is changed. </summary>
        public event EventHandler<EventArgs> SelectionChanged;

        /// <summary> Selection changed. </summary>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void OnSelectionChanged( EventArgs e )
        {
            var evt = SelectionChanged;
            evt?.Invoke( this, e );
        }

        /// <summary> Entity collection grid selection changed. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void EntityCollectionGrid_SelectionChanged( object sender, EventArgs e )
        {
            this.OnSelectionChanged( e );
        }


#endregion

#region " REFRESH EVENTS "

        /// <summary> Requests the display update. </summary>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public bool RequestDisplayUpdate()
        {
            this.OnEntityCollectionUpdateRequested( EventArgs.Empty );
            return true;
        }

        /// <summary>
        /// Handles the Click event of the _RefreshButton control.
        /// Request a refresh of the display.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void RefreshButton_Click( object sender, EventArgs e )
        {
            // 2014-06-23 Me._EntityCollectionGrid.DataSource = Nothing
            this.UpdateBottomStatusMessage( "Refresh of {0} requested", this.EntityCollectionTitle );
            this.OnRefreshRequested( EventArgs.Empty );
            this.BottomStatusMessage = "Refresh processed";
        }

#endregion

#region " PRINTING "


        /// <summary> Occurs when a Print display is requested. </summary>
        public event EventHandler<EventArgs> PrintRequested;

        /// <summary> Removes print requested event. </summary>
        /// <param name="value"> The value. </param>
        private void RemovePrintRequestedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    PrintRequested -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Print requested. </summary>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void OnPrintRequested( EventArgs e )
        {
            var evt = PrintRequested;
            evt?.Invoke( this, e );
        }

        /// <summary> Request printing. </summary>
        private void PrintButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"Printing {this.EntityCollectionTitle}";
                this._ErrorProvider.Clear();
                this.UpdateBottomStatusMessage( activity );
                this.OnPrintRequested( EventArgs.Empty );
                this.BottomStatusMessage = "Printing processed";
            }
            catch ( Exception ex )
            {
                this.Enunciate( this._BottomToolStrip, "Exception occurred printing" );
                this.BottomStatusMessage = "Exception occurred requesting printing";
                _ = this.PublishException( activity, ex );
            }
        }

#endregion

#region " SAVING "

        /// <summary> Gets the sentinel indicated if the grid collection has dirty entities. </summary>
        /// <value> <c>True</c> if the grid collection has dirty entities. </value>
        public bool IsDirty => this.EntityCollectionGrid is object && this.EntityCollectionGrid.IsDirty;

        /// <summary> Saves changes to collection. </summary>
        private void SaveButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                int itemCount = 0;
                this.Cursor = Cursors.WaitCursor;
                if ( this.EntityCollectionGrid.IsDirty )
                {
                    this.UpdateBottomStatusMessage( "Saving changes in {0}...", this.EntityCollectionTitle );
                    _ = (this.Talker?.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Saving {0};. ", this.EntityCollectionTitle ));
                    itemCount = this.EntityCollectionGrid.SaveChanges();
                    this.UpdateBottomStatusMessage( "Saving completed--{0} {1} items saved and/or removed.", this.EntityCollectionTitle, itemCount );
                    _ = (this.Talker?.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Saving of {0} completed; {1} items saved and/or removed;. ", this.EntityCollectionTitle, itemCount ));
                }

                if ( itemCount > 0 )
                {
                    this.OnEntityCollectionSaved( EventArgs.Empty );
                }
            }
            catch ( Exception ex )
            {
                this.Enunciate( this._BottomToolStrip, "Exception occurred saving" );
                this.UpdateBottomStatusMessage( "Exception occurred saving/deleting {0}", this.EntityCollectionTitle );
                _ = (this.Talker?.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred saving/deleting '{0}';. {1}", this.EntityCollectionTitle, ex.ToFullBlownString() ));
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        /// <summary> Shows or hides the Save button. </summary>
        /// <value> The Action Visible sentinel. </value>
        [Category( "Behavior" )]
        [Description( "Shows or hides the Save button." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool SaveVisible
        {
            get => this._SaveButton.Visible;

            set {
                if ( !value.Equals( this.SaveVisible ) )
                {
                    this._SaveButton.Visible = value;
                    this._SaveButtonSeparator.Visible = value;
                }
            }
        }

#endregion

#region " ACTION "

        /// <summary> Gets or set the name of the actions button. </summary>
        [Category( "Appearance" )]
        [Description( "The title of the actions button." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "" )]
        public string ActionsTitle
        {
            get => this._ActionsButton.Text;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                SafeTextSetter( this._ActionsButton, value );
                this._ActionsButton.DisplayStyle = string.IsNullOrWhiteSpace( value ) ? ToolStripItemDisplayStyle.Image : ToolStripItemDisplayStyle.ImageAndText;
            }
        }

        /// <summary> Enables or disables actions. </summary>
        /// <value> The Action enabled sentinel. </value>
        [Category( "Behavior" )]
        [Description( "Enables or disables actions." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool ActionsEnabled
        {
            get => this._ActionsButton.Enabled;

            set {
                if ( !value.Equals( this.ActionsEnabled ) )
                {
                    this._ActionsButton.Enabled = value;
                    this._ActionsButtonSeparator.Enabled = value;
                }
            }
        }

        /// <summary> Shows or hides the actions button. </summary>
        /// <value> The Action Visible sentinel. </value>
        [Category( "Behavior" )]
        [Description( "Shows or hides the actions button." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool ActionsVisible
        {
            get => this._ActionsButton.Visible;

            set {
                if ( !value.Equals( this.ActionsVisible ) )
                {
                    this._ActionsButton.Visible = value;
                    this._ActionsButtonSeparator.Visible = value;
                }
            }
        }

        /// <summary> Occurs when a Action is requested. </summary>
        public event EventHandler<EventArgs> ActionRequested;

        /// <summary> Removes action requested event. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveActionRequestedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    ActionRequested -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the action requested event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnActionRequested( EventArgs e )
        {
            var evt = ActionRequested;
            evt?.Invoke( this, e );
        }

        /// <summary> Event handler. Called by _ActionsButton for click events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ActionsButton_Click( object sender, EventArgs e )
        {
            this.BottomStatusMessage = "Action requested...";
            this.OnActionRequested( EventArgs.Empty );
            this.SavingEnabled = this.SavingEnabled || this.IsDirty;
            this.BottomStatusMessage = "Action processed";
        }

#endregion

#region " EXPORT "

        /// <summary> Gets or set the name of the Export button. </summary>
        [Category( "Appearance" )]
        [Description( "The title of the Export button." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "" )]
        public string ExportTitle
        {
            get => this._ExportButton.Text;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( this._ExportButton is object && !string.Equals( value, this.ExportTitle ) )
                {
                    this.ExportEnabled = !string.IsNullOrWhiteSpace( value );
                    SafeTextSetter( this._ExportButton, value );
                    this._ExportButton.DisplayStyle = string.IsNullOrWhiteSpace( value ) ? ToolStripItemDisplayStyle.Image : ToolStripItemDisplayStyle.ImageAndText;
                }
            }
        }

        /// <summary> Enables or disables Export. </summary>
        /// <value> The Export enabled sentinel. </value>
        [Category( "Behavior" )]
        [Description( "Enables or disables Export." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool ExportEnabled
        {
            get => this._ExportButton.Enabled;

            set {
                if ( this._ExportButton is object && !value.Equals( this.ExportEnabled ) )
                {
                    this._ExportButton.Enabled = value;
                    this._ExportButtonSeparator.Enabled = value;
                }
            }
        }

        /// <summary> Shows or hides the Export button. </summary>
        /// <value> The Action Visible sentinel. </value>
        [Category( "Behavior" )]
        [Description( "Shows or hides the Export button." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool ExportVisible
        {
            get => this._ExportButton.Visible;

            set {
                if ( !value.Equals( this.ExportVisible ) )
                {
                    this._ExportButton.Visible = value;
                    this._ExportButtonSeparator.Visible = value;
                }
            }
        }

        /// <summary> Occurs when a Export is requested. </summary>
        public event EventHandler<EventArgs> ExportRequested;

        /// <summary> Removes export requested event. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveExportRequestedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    ExportRequested -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the Export requested event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnExportRequested( EventArgs e )
        {
            var evt = ExportRequested;
            evt?.Invoke( this, e );
        }

        /// <summary> Event handler. Called by _ExportButton for click events. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ExportButton_Click( object sender, EventArgs e )
        {
            this.UpdateBottomStatusMessage( "Export of {0} requested", this.EntityCollectionTitle );
            this.OnExportRequested( EventArgs.Empty );
            this.BottomStatusMessage = "Export processed";
        }

#endregion


#region " MESSAGES "

        /// <summary> Enunciates the specified control. </summary>
        /// <param name="control"> The control. </param>
        /// <param name="format">  The format. </param>
        /// <param name="args">    The arguments. </param>
        private void Enunciate( Control control, string format, params object[] args )
        {
            this._ErrorProvider.Clear();
            this._ErrorProvider.SetError( control, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        /// <summary> Updates the bottom status label. </summary>
        /// <param name="format">   The format. </param>
        /// <param name="args">     The arguments. </param>
        public void UpdateBottomStatusMessage( string format, params object[] args )
        {
            this.BottomStatusMessage = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
        }

        /// <summary> Gets or sets a message describing the bottom status. </summary>
        /// <value> A message describing the bottom status. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string BottomStatusMessage
        {
            get => this._BottomStatusLabel.Text;

            set => SafeTextSetter( this._BottomStatusLabel, value );
        }

#endregion

#region " SAFE SETTERS "

        /// <summary> Safe text setter. </summary>
        /// <param name="control"> The control. </param>
        /// <param name="value">   The synopsis to display. </param>
        private static void SafeTextSetter( Control control, string value )
        {
            if ( string.IsNullOrEmpty( value ) )
                value = string.Empty;
            if ( control is object )
            {
                if ( control.InvokeRequired )
                {
                    _ = control.Invoke( new Action<Control, string>( SafeTextSetter ), new object[] { control, value } );
                }
                else
                {
                    control.Text = value;
                }
            }
        }

        /// <summary> Safe text setter. </summary>
        /// <param name="control"> The control. </param>
        /// <param name="value">   The synopsis to display. </param>
        private static void SafeTextSetter( ToolStripItem control, string value )
        {
            if ( string.IsNullOrEmpty( value ) )
                value = string.Empty;
            if ( control is object )
            {
                if ( control.Owner.InvokeRequired )
                {
                    _ = control.Owner.Invoke( new Action<Control, string>( SafeTextSetter ), new object[] { control, value } );
                }
                else
                {
                    control.Text = value;
                }
            }
        }

#endregion

#region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> Declared as must override so that the Trace Event Id of the caller library is used </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> Declared as must override so that the To Full Blown String is extended based on the caller library implementation </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

#endregion

    }
}
