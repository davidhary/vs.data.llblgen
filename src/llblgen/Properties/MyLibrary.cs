using System.ComponentModel;

namespace isr.Data.LLBLGen.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    public sealed partial class MyLibrary
    {

        /// <summary> Constructor that prevents a default instance of this class from being created. </summary>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = 546;
        /// <summary>   (Immutable) the assembly title. </summary>
        public const string AssemblyTitle = "LLBLGen ORM Library";
        /// <summary>   (Immutable) information describing the assembly. </summary>
        public const string AssemblyDescription = "LLBLGen Object Relation Mapping Library";
        /// <summary>   (Immutable) the assembly product. </summary>
        public const string AssemblyProduct = "LLBLGen";
    }

    /// <summary> Values that represent project trace event identifiers. </summary>
    public enum ProjectTraceEventId
    {
        /// <summary>   An enum constant representing the none option. </summary>
        [Description( "Not specified" )]
        None,

        /// <summary>   An enum constant representing the llbl Generate core option. </summary>
        [Description( "LLBLGen Core" )]
        LLBLGenCore = Core.ProjectTraceEventId.Data + 0x2,

        /// <summary>   An enum constant representing the llbl Generate publishers option. </summary>
        [Description( "LLBLGen Publishers" )]
        LLBLGenPublishers = Core.ProjectTraceEventId.Data + 0x3,

        /// <summary>   An enum constant representing the llbl Generate self servicing option. </summary>
        [Description( "LLBLGen Self Servicing" )]
        LLBLGenSelfServicing = Core.ProjectTraceEventId.Data + 0x4
    }
}
