﻿using System;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace isr.Data.LLBLGen.PrefetchPathExtensions
{
    /// <summary> Prefetch path Extensions. </summary>
    /// <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class Methods
    {

        /// <summary> Determines whether the prefetch path contains the specified path. </summary>
        /// <param name="path">      The path. </param>
        /// <param name="candidate"> The candidate. </param>
        /// <returns> <c>True</c> if the prefetch path contains the specified path; otherwise,
        /// <c>False</c>. </returns>
        public static bool Contains( this IPrefetchPath path, IPrefetchPathElement candidate )
        {
            if ( path is null || candidate is null )
            {
                return false;
            }
            else if ( path.Count == 0 )
            {
                return false;
            }
            else
            {
                for ( int i = 0, loopTo = path.Count - 1; i <= loopTo; i++ )
                {
                    if ( path[i].Equals( candidate ) )
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary> Adds the specified candidate path or selects it if it already exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="path">      The path. </param>
        /// <param name="candidate"> The candidate. </param>
        /// <returns> <see cref="IPrefetchPathElement">Prefetch path element.</see> </returns>
        public static IPrefetchPathElement AddSelect( this IPrefetchPath path, IPrefetchPathElement candidate )
        {
            if ( path is null )
            {
                throw new ArgumentNullException( nameof( path ) );
            }
            else
            {
                return path.Contains( candidate ) ? candidate : path.Add( candidate );
            }
        }

        /// <summary> Adds the specified candidate path or selects it if it already exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="path">             The path. </param>
        /// <param name="candidate">        The candidate. </param>
        /// <param name="additionalSorter"> The additional sorter. </param>
        /// <returns> <see cref="IPrefetchPathElement">Prefetch path element.</see> </returns>
        public static IPrefetchPathElement AddSelect( this IPrefetchPath path, IPrefetchPathElement candidate, ISortExpression additionalSorter )
        {
            if ( path is null )
            {
                throw new ArgumentNullException( nameof( path ) );
            }
            else
            {
                return path.Contains( candidate ) ? candidate : path.Add( candidate, default, null, null, additionalSorter );
            }
        }

        /// <summary> Adds the specified candidate path or selects it if it already exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="path">                      The path. </param>
        /// <param name="candidate">                 The candidate. </param>
        /// <param name="maxAmountOfItemsToReturn">  The max amount of items to return. </param>
        /// <param name="additionalFilter">          The additional filter. </param>
        /// <param name="additionalFilterRelations"> The additional filter relations. </param>
        /// <param name="additionalSorter">          The additional sorter. </param>
        /// <returns> <see cref="IPrefetchPathElement">Prefetch path element.</see> </returns>
        public static IPrefetchPathElement AddSelect( this IPrefetchPath path, IPrefetchPathElement candidate, int maxAmountOfItemsToReturn, IPredicateExpression additionalFilter, IRelationCollection additionalFilterRelations, ISortExpression additionalSorter )
        {
            if ( path is null )
            {
                throw new ArgumentNullException( nameof( path ) );
            }
            else
            {
                return path.Contains( candidate ) ? candidate : path.Add( candidate, maxAmountOfItemsToReturn, additionalFilter, additionalFilterRelations, additionalSorter );
            }
        }
    }
}