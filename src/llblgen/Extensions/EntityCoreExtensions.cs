using SD.LLBLGen.Pro.ORMSupportClasses;

namespace isr.Data.LLBLGen.EntityCoreExtensions
{

    /// <summary> Includes extensions for LLBLGEN Core Entities. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975.x. </para></remarks>
    public static class Methods
    {

        #region " ENTITY CORE "

        /// <summary> Determines if the entity is not new or dirty. </summary>
        /// <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        /// interface</see> </param>
        /// <returns> <c>True</c>  if the entity is not new or dirty; Otherwise, <c>False</c>. </returns>
        public static bool IsClean( this IEntityCore entity )
        {
            return entity is object && !(entity.IsNew || entity.IsDirty);
        }

        /// <summary> Query if 'entity' is stored. </summary>
        /// <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        ///                       interface</see> </param>
        /// <returns> <c>true</c> if stored; otherwise <c>false</c> </returns>
        public static bool IsStored( this IEntityCore entity )
        {
            return entity is object && !entity.IsNew;
        }

        /// <summary> Query if 'entity' is nothing or new. </summary>
        /// <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        ///                       interface</see> </param>
        /// <returns> <c>true</c> if nothing or new; otherwise <c>false</c> </returns>
        public static bool IsNothingOrNew( this IEntityCore entity )
        {
            return entity is null || entity.IsNew;
        }

        /// <summary> Query if 'entity' is not stored. </summary>
        /// <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        ///                       interface</see> </param>
        /// <returns> <c>true</c> if not stored; otherwise <c>false</c> </returns>
        public static bool IsNotStored( this IEntityCore entity )
        {
            return entity is object && entity.IsNew;
        }

        #endregion

    }
}
