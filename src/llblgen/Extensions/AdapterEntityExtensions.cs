using System.Collections.Generic;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace isr.Data.LLBLGen.AdapterEntityExtensions
{

    /// <summary> Includes extensions for Adapter LLBLGEN Entities. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975.x. </para></remarks>
    public static class Methods
    {

        #region " ADAPTER ENTITY (IEntity2) "

        /// <summary> Returns the list of field names. </summary>
        /// <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        /// interface</see> </param>
        /// <returns> The field names. </returns>
        public static string[] GetFieldNames( this IEntity2 entity )
        {
            var l = new List<string>();
            if ( entity is object )
            {
                foreach ( IEntityField2 field in entity.Fields )
                    l.Add( field.Name );
            }

            return l.ToArray();
        }

        /// <summary> Returns the list of primary key field names. </summary>
        /// <param name="entity"> Reference to an entity implementing the <see cref="IEntity">entity
        /// interface</see> </param>
        /// <returns> The primary key field names. </returns>
        public static string[] GetPrimaryKeyFieldNames( this IEntity2 entity )
        {
            var l = new List<string>();
            if ( entity is object )
            {
                foreach ( IEntityField2 field in entity.PrimaryKeyFields )
                    l.Add( field.Name );
            }

            return l.ToArray();
        }

        #endregion

    }
}
