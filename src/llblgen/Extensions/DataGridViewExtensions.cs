using System.Linq;
using System.Windows.Forms;

using isr.Data.LLBLGen.AdapterEntityExtensions;
using isr.Data.LLBLGen.SelfServicingEntityExtensions;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace isr.Data.LLBLGen.DataGridViewExtensions
{

    /// <summary> Includes data grid view extensions for LLBLGEN Entities. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2010-11-19, 1.2.3975.x. </para></remarks>
    public static class Methods
    {

        #region " GRID "

        /// <summary> Hides all columns. </summary>
        /// <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        public static void HideColumns( this DataGridView grid )
        {
            if ( grid is object )
            {
                foreach ( DataGridViewColumn column in grid.Columns )
                    column.Visible = false;
            }
        }

        /// <summary> Hides the columns by the <paramref name="columnNames">column names</paramref>. </summary>
        /// <param name="grid">        The grid. </param>
        /// <param name="columnNames"> The column names. </param>
        public static void HideColumns( this DataGridView grid, string[] columnNames )
        {
            if ( grid is object )
            {
                foreach ( DataGridViewColumn column in grid.Columns )
                {
                    if ( !columnNames.Contains( column.Name ) )
                    {
                        column.Visible = false;
                    }
                }
            }
        }

        /// <summary> Visible column count. </summary>
        /// <param name="grid"> The <see cref="DataGridView">grid</see> </param>
        /// <returns> The visible column count. </returns>
        public static int VisibleColumnCount( this DataGridView grid )
        {
            int count = 0;
            if ( grid is object )
            {
                foreach ( DataGridViewColumn column in grid.Columns )
                {
                    if ( column.Visible )
                    {
                        count += 1;
                    }
                }
            }

            return count;
        }

        #endregion

        #region " ADAPTER "

        /// <summary> Hides columns that are not defined as entity fields. </summary>
        /// <param name="grid">   The <see cref="DataGridView">grid</see> </param>
        /// <param name="entity"> Specifies the entity displayed in the data grid. </param>
        public static void HideColumnsByEntityFields( this DataGridView grid, IEntity2 entity )
        {
            if ( grid is object )
            {
                grid.HideColumns( entity.GetFieldNames() );
            }
        }

        #endregion

        #region " SELF SERVICING "

        /// <summary> Hides columns that are not defined as entity fields. </summary>
        /// <param name="grid">   The <see cref="DataGridView">grid</see> </param>
        /// <param name="entity"> Specifies the entity displayed in the data grid. </param>
        public static void HideColumnsByEntityFields( this DataGridView grid, IEntity entity )
        {
            if ( grid is object )
            {
                grid.HideColumns( entity.GetFieldNames() );
            }
        }

        #endregion

    }
}
